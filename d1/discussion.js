// objects to be used in the discussion
db.fruits.insertMany([{
        name: "Apple",
        color: "Red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: ["Philippines", "US"]
    },

    {
        name: "Banana",
        color: "Yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: ["Philippines", "Ecuador"]
    },

    {
        name: "Kiwi",
        color: "Green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: ["US", "China"]
    },

    {
        name: "Mango",
        color: "Yellow",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: ["Philippines", "India"]
    }
]);

// SECTION - MongoDB Aggregation
/*
	- used to generate manipulated data and perform operations to create filtered results that help in analyzing data
	- Compared CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application
*/

// SECTION - USING  AGGREGATE METHOD
/*
	$match
		- used to pass the documents that meet the specified condition(s) to the next pipeline/aggregation process

		- pipelines/aggregation process is the series of aggregation methods should the dev/client wants to use two or more aggregation methods in one statement. MongoDB will treat this as stages  wherein it will not proceed to the next pipeline, unless it is done with the first

		SYNTAX:
			{ $match : { field : value } }
*/
db.fruits.aggregate([
    { $match: { onSale: true } }
]);

/*
	$group
		-used to group elements together and field-value pairs using the data from the grouped elements
		SYNTAX:
			{ $group : { _id: "$value", fieldResult: "$valueResult" } }
*/
db.fruits.aggregate([
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);

/*
	using both $match and $group along with aggregation will find for the products that are on sale and will group the total amount of stocks for all suppliers found (this will be done in that specific order)

	SYNTAX:
		db.collectionName.aggregate([
			{ $match : { field : value } },
			{ $group : { _id: "$value", fieldResult: "$valueResult" } }
		]);
*/
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);

// Field projections with aggregation
/*
	$project
		- can be used when aggregating data to include/exclude fields from the returned result
		SYNTAX:
			{ $project: { field : 1/0 } }
*/
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $project: { _id: 0 } }
]);

/*
	$sort
		- used to change the order of aggregated results
		- providing -1 as the value will result to MongoDB sorting the documents in reversed order
		SYNTAX:
			{ $sort: { field: 1/-1 } }
*/
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $sort: { total: -1 } }
]);

/*
	code below won't work if the goal is to group the documents based on the number of times a country is mentioned in the "origin" array field
	db.fruits.aggregate([
		{ $group : {_id : "$origin", kinds: {$sum : 1}} }
	]);	
*/
// $unwind
/*
	- used to deconstruct an array field from a collection/field with array value to output a result for each element.
	- this will result to creating separate documents for each of the elements inside the array
	SYNTAX:
		{ $unwind : field }
*/
db.fruits.aggregate([
    { $unwind:: "$origin" }
]);
// Using the code below, we are instructing MongoDB to deconstruct first the documents based on the origin field and group them based on the number of times a country is mentioned in the "origin" field
db.fruits.aggregate([
    { $unwind: "$origin" },
    { $group: { _id: "$origin", kinds: { $sum: 1 } } }
]);

// SECTION - Schema Design
/*
	- schema design/data modelling is an important feature when creating databases
	- MongoDB documents can be categorized into nomalized or de-normalized/embedded data
	- Normalized data refers to a data structure where documents are reffered to each other using their ids for related pieces of information
	- De-normalized data/embedded data design refers to a data structure where related pieces of information are added to a document as an embedded object
*/

// Normalized data schema design in one-to-one relationship
var owner = ObjectId();

db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact: "09123456789"
});
/*
	miniactivity
		create "suppliers" collection and insert an object with the following properties
			- name - string
			- contact - string
			- owner_id - similar id as our "John Smith" id
		send the output in the google chat
*/
db.suppliers.insert({
    name: "ABC fruits",
    contact: "09123456789",
    owner_id: ObjectId("62d54756452148fa5c33a75e")
});

/*
COMPARISON OF THE TWO IDS
	John Smith's id
		 ObjectId("62d54756452148fa5c33a75e")
	Supplier.owner_id
		 ObjectId("62d54756452148fa5c33a75e")
*/

// de-normalized data schema design with one-to-few relationship
db.suppliers.insert({
    name: "DEF fruits",
    contact: "09123456789",
    address: [
        { street: "123 San Jose St.", city: "Manila" },
        { street: "367 Gil Puyat", city: "Makati" }
    ]
});

/*
	-Both data structures are common practices but each of them has its own pros and cons

Difference between Normalized and De-normalized
NORMALIZED
	- it makes it easier to read information because separate documents can be retrieved.

	- in terms of querying results, it performs slower compared to embedded data due to having to retrieve multiple documents at the same time.
	
	- This approach is useful for  data structures where pieces of information are common operated on/changed
	
DE-NORMALIZED
	- it makes it easier to query documents and has a faster performance because only one query needs to be done in order to retrieve the document/s.

	- if the data structure becomes too complex and long it makes it more difficult to manipulate and access information

	- This approach is applicable for data structures where pieces of information are commonly retrieved and rearely operated on/changed
*/

/*
	Miniactivity
		suppliers collection:
			create 2 variable with ObjectId() values
				supplier
				branch

			insert a document with the following properties
				_id: supplier,
				name: string,
				contact: string,
				branches: [
					branch
				]

		create a branch collection and insert an object with the following properties
			_id: similar as the "branch" field in the suppliers collection
			name: string,
			address: string,
			city: string,
			supplier_id: the same id as the supplier in supplier collection

		send the output in the google chat.
*/