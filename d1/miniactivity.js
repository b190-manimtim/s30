db.rooms.insertMany([{

        name: "apple",
        color: "red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: ["Philippines", "US"]
    },
    {
        name: "banana",
        color: "yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: ["Philippines", "Ecuador"]
    },
    {
        name: "kiwi",
        color: "green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: ["US", "China"]
    },
    {
        name: "mango",
        color: "green",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: ["Philippines", "India"]
    }

]);